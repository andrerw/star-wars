import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Import Route Module
import { RouteModule } from './route.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

// Import All Component file
import { AppComponent } from './app.component';
import {
    FilmComponent,
    AddFilmComponent,
    DetailFilmComponent,
    SearchFilmComponent,
    FilmService
} from './film';

@NgModule({
  declarations: [
    AppComponent,
    FilmComponent,
    AddFilmComponent,
    DetailFilmComponent,
    SearchFilmComponent
  ],
  imports: [
    BrowserModule,
    RouteModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    InMemoryWebApiModule.forRoot(InMemoryDataService)
  ],
  providers: [
      FilmService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
