import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import { FilmService } from './film.service';

@Component({
    selector: 'add-film',
    templateUrl: './add-film.component.html'
})

export class AddFilmComponent {
    dataFilm = {
        title: '',
        opening_crawl: '',
        producer: ''
    }

    constructor(
        private filmService: FilmService,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    goBack(): void {
        this.location.back();
    }

    save(): void {
        let data = this.dataFilm;
        this.filmService.create(data)
            .then(response => {
                console.log(response);
                this.goBack();
            });
    }
}
