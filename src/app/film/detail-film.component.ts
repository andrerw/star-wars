import 'rxjs/add/operator/switchMap';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import { FilmService } from './film.service';

@Component({
    selector: 'film-detail',
    templateUrl: './detail-film.component.html'
})

export class DetailFilmComponent implements OnInit {
    @Input() film: any;

    constructor(
        private filmService: FilmService,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    goBack(): void {
        this.location.back();
    }

    save(): void {
        this.filmService.update(this.film)
            .then(() => this.goBack());
    }

    ngOnInit(): void {
        this.route.paramMap
            .switchMap((params: ParamMap) => this.filmService.getDetailFilm(+params.get('id')))
            .subscribe(film => this.film = film);
    }
}
