import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { FilmService } from './film.service';

@Component({
    selector: 'starwars-film',
    templateUrl: './film.component.html'
})

export class FilmComponent implements OnInit {
    films: any[];
    closeResult: string;
    dataModal: any;

    constructor(
        private filmService: FilmService,
        private router: Router,
        private modalService: NgbModal
    ) {}

    getAllFilms(): void {
        this.filmService.getAllFilms()
            .then(response => this.films = response);
    }

    gotoDetailFilm(films: any, modal): void {
        this.dataModal = films;

        this.modalService.open(modal).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    addFilm(): void {
        this.router.navigate(['/add-film']);
    }

    editFilm(films): void {
        this.router.navigate(['/film-detail', films.id]);
    }

    deleteFilm(film: any): void {
        this.filmService
            .delete(film.id)
            .then(() => {
                this.films = this.films.filter(h => h !== film);
            });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    ngOnInit(): void {
        this.getAllFilms();
    }
}
