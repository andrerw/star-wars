import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FilmService {
    // private filmsUrl = 'https://swapi.co/api/films';
    private filmsUrl = 'api/films';  // URL to web api
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(
        private http: Http
    ) {}

    getAllFilms(): Promise<any[]> {
        return this.http.get(`${environment}/films`)
            .toPromise()
            .then(response => response.json().data)
            .catch(error => console.error(error));
    }

    getDetailFilm(id: number): Promise<any> {
        return this.http.get(`${environment}/films/${id}`)
            .toPromise()
            .then(response => response.json().data)
            .catch(error => console.error(error));
    }

    create(data: any): Promise<any> {
        return this.http
            .post(`${environment}/films`, JSON.stringify(data), {headers: this.headers})
            .toPromise()
            .then(response => response.json().data)
            .catch(error => console.error(error));
    }

    update(data: any): Promise<any> {
      const url = `${environment}/films/${data.id}`;
      return this.http
        .put(url, JSON.stringify(data), {headers: this.headers})
        .toPromise()
        .then(() => data)
        .catch(error => console.error(error));
    }

    delete(id: number): Promise<void> {
        return this.http
            .delete(`${environment}/films/${id}`, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(error => console.error(error));
    }

    search(term: string): Observable<any> {
        return this.http
            .get(`${this.filmsUrl}/?title=${term}`)
            .map(response => response.json().data);
    }
}
