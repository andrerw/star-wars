export * from './film.component';
export * from './add-film.component';
export * from './detail-film.component';
export * from './search-film.component';
export * from './film.service';
