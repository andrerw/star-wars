import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

// Observable
import "rxjs/add/observable/of";

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { FilmService } from './film.service';

@Component({
    selector: 'search-film',
    templateUrl: './search-film.component.html',
    styleUrls: ['./search-film.component.css'],
    providers: [FilmService]
})

export class SearchFilmComponent implements OnInit {
    private searchTerms = new Subject<string>();
    films: Observable<any[]>;

    constructor(
        private filmService: FilmService,
        private router: Router
    ) {}

    search(term: string): void {
        this.searchTerms.next(term);
    }

    editFilm(film: any): void {
        let link = ['/film-detail', film.id];
        this.router.navigate(link);
    }

    ngOnInit(): void {
        this.films = this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term => term ? this.filmService.search(term) : Observable.of<any[]>([]))
            .catch(error => {
                console.log(error);
                return Observable.of<any[]>([]);
            });
    }
}
