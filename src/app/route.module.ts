import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FilmComponent } from './film/film.component';
import { AddFilmComponent } from './film/add-film.component';
import { DetailFilmComponent } from './film/detail-film.component';

const routes: Routes = [
    { path: 'film', component: FilmComponent },
    { path: 'add-film', component: AddFilmComponent },
    { path: 'film-detail/:id', component: DetailFilmComponent },
    { path: '', redirectTo: '/film', pathMatch: 'full' },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class RouteModule {}
