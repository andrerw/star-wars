export class Films {
    id: number;
    title: string;
    image: string;
    producer: string;
    years: number;
}
