import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';

// import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// import { JwtService } from './jwt.service';

@Injectable()
export class ApiService {
    constructor(
        private http: Http
        // private jwtService: JwtService
    ) {}

    private setHeaders(): Headers {
        const headersConfig = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        // if(this.jwtService.getToken()) {
        //     headersConfig['Authorization'] = `Token ${this.jwtService.getToken()}`;
        // }
        return new Headers(headersConfig);
    }

    private formatErrors(error: any) {
        return Observable.throw(error.json());
    }

    get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
        return this.http.get(`${environment.api_url}${path}`, {headers: this.setHeaders(), search: params})
            .map((response: Response) => response.json())
            .catch(this.formatErrors);
    }

    post(path: string, body: Object = {}): Observable<any> {
        return this.http.post(`${environment.api_url}${path}`, JSON.stringify(body), {headers: this.setHeaders()})
            .map((response: Response) => response.json())
            .catch(this.formatErrors);
    }

    put(path: string, body: Object = {}): Observable<any> {
        return this.http.put(`${environment.api_url}${path}`, JSON.stringify(body), {headers: this.setHeaders()})
            .map((response: Response) => response.json())
            .catch(this.formatErrors);
    }

    delete(path): Observable<any> {
        return this.http.delete(`${environment.api_url}${path}`, {headers: this.setHeaders()})
            .map((response: Response) => response.json())
            .catch(this.formatErrors);
    }

    // get(path: string, params: URLSearchParams = new URLSearchParams()): Promise<any> {
    //     return this.http
    //         .get(`${environment}${path}`, {headers: this.setHeaders(), search: params})
    //         .toPromise()
    //         .then((response: Response) => response.json())
    //         .catch(this.formatErrors);
    // }
    //
    // post(path: string, body: Object = {}): Promise<any> {
    //     return this.http
    //         .post(`${environment}${path}`, JSON.stringify(body), {headers: this.setHeaders()})
    //         .toPromise()
    //         .then((response: Response) => response.json())
    //         .catch(this.formatErrors);
    // }
    //
    // put(path: string, body: Object = {}): Promise<any> {
    //     return this.http
    //         .put(`${environment}${path}`, JSON.stringify(body), {headers: this.setHeaders()})
    //         .toPromise()
    //         .then((response: Response) => response.json())
    //         .catch(this.formatErrors);
    // }
    //
    // delete(path): Promise<any> {
    //     return this.http
    //         .delete(`${environment}${path}`, {headers: this.setHeaders()})
    //         .toPromise()
    //         .then((response: Response) => response.json())
    //         .catch(this.formatErrors);
    // }
}
