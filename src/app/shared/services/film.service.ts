import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';

@Injectable()
export class FilmService {
    constructor(
        private apiService: ApiService
    ) {}

    getAll(): Observable<any[]> {
        return this.apiService.get('/films').map(data => data);
    }

    getDetail(id): Observable<any> {
        return this.apiService.get('/films/' + id).map(data => data);
    }

    destroy(id) {
        return this.apiService.delete('/films/' + id);
    }

    save(film): Observable<any[]> {
        if(film.id) {
            return this.apiService.put('/films/' + film.id)
                .map(data => data);
        } else {
            return this.apiService.post('/films/')
                .map(data => data);
        }
    }
}
